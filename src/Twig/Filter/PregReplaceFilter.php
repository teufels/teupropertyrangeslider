<?php

namespace TeuPropertyRangeSlider\Twig\Filter;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PregReplaceFilter extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('preg_replace', [$this, 'pregReplace']),
        ];

    }

    public function pregReplace($subject, $pattern, $replacement): string
    {
        return preg_replace($pattern, $replacement, $subject);
    }
}
