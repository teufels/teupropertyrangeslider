<?php declare(strict_types=1);

namespace TeuPropertyRangeSlider;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollectionBuilder;

class TeuPropertyRangeSlider extends Plugin
{
    // TODO: refactor to separate files + uninstall field
    private const SW_FIELD_COMPONENT_NAME = 'sw-field';

    public function install(InstallContext $context): void
    {
        $this->checkCustomFields($context);
    }

    public function update(UpdateContext $context): void
    {
        $this->checkCustomFields($context);
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        //$this->container = $container;
    }

    public function getViewPaths(): array
    {
        return [
            '/Resources/views'
        ];
    }


    private function checkCustomFields($context)
    {

        $customCatFields = [
            [
                'id' => Uuid::fromBytesToHex('teuPropRangeSet0'),
                'name' => 'teu_custom_prop_rangeslider_set',
                'active' => true,
                'config' => [
                    'label' => [
                        'en-GB' => 'teu rangeslider setting',
                        'de-DE' => 'teu Rangeslider Eigenschaften'
                    ],
                ],
                'customFields' => [
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeUnit'),
                        'name' => 'teu_cat_prop_rangeslider_unit',
                        'type' => \Shopware\Core\System\CustomField\CustomFieldTypes::TEXT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'range unit',
                                'de-DE' => 'Range Einheit',
                            ]
                        ]
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeKey0'),
                        'name' => 'teu_cat_prop_rangeslider_key',
                        'type' => \Shopware\Core\System\CustomField\CustomFieldTypes::TEXT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'range key',
                                'de-DE' => 'Range Key',
                            ]
                        ]
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeStep'),
                        'name' => 'teu_cat_prop_rangeslider_steps',
                        'type' => \Shopware\Core\System\CustomField\CustomFieldTypes::FLOAT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'range steps',
                                'de-DE' => 'Range Steps',
                            ]
                        ]
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeDeci'),
                        'name' => 'teu_cat_prop_rangeslider_decimals',
                        'type' => \Shopware\Core\System\CustomField\CustomFieldTypes::FLOAT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'range decimals',
                                'de-DE' => 'Range Nachkommastellen',
                            ]
                        ]
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('teuCsvImportGen3'),
                        'name' => 'teu_cat_prop_rangeslider_displayForConfigurator',
                        'type' => \Shopware\Core\System\CustomField\CustomFieldTypes::SELECT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'display type for configurator',
                                'de-DE' => 'Darstellung im Konfigurator',
                            ],
                            'options' => [
                                [
                                    'value' => 'text',
                                    'label' => [
                                        'de-DE' => 'Text',
                                        'en-GB' => 'text',
                                    ]
                                ],
                                [
                                    'value' => 'select',
                                    'label' => [
                                        'de-DE' => 'Dropdown',
                                        'en-GB' => 'Dropdown',
                                    ]
                                ],
                            ],
                            'componentName' => 'sw-single-select',
                            'customFieldType' => 'select',
                        ],
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeSing'),
                        'name' => 'teu_cat_prop_rangeslider_useSingleRange',
                        'type' => CustomFieldTypes::BOOL,
                        'config' => [
                            'label' => [
                                'en-GB' => 'use single range',
                                'de-DE' => 'Als Single Range anzeigen',
                            ],
                            'componentName' => 'sw-field',
                            'customFieldType' => 'switch',
                        ]
                    ],
                ],
                'relations' => [
                    [
                        'id' => Uuid::fromBytesToHex('teuPropRangeSet0'),
                        'entityName' => 'property_group'
                    ]
                ]
            ]
        ];

        /** @var EntityRepository $repo */
        $repo = $this->container->get('custom_field_set.repository');

        foreach ($customCatFields as $customCatFieldSet) {
            $repo->upsert([$customCatFieldSet], $context->getContext());
        }
    }
}
