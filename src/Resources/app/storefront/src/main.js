import FilterRangeSliderPlugin from './script/filter-range-slider.plugin';
import FilterRangeExtendPlugin from './script/plugin/listing/filter-range.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('FilterRangeSlider', FilterRangeSliderPlugin, '[data-filter-range-slider]');
//PluginManager.override('FilterRange', FilterRangeExtendPlugin, '[data-filter-range]');
