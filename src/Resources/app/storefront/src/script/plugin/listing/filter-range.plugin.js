import FilterRangePlugin from 'src/plugin/listing/filter-range.plugin';

export default class FilterRangeExtendPlugin extends FilterRangePlugin {

    getLabels() {
        let labels = [];

        const decimals = this.options.decimals;

        const formatOptions = {
            minimumFractionDigits: decimals,
            maximumFractionDigits: decimals
        }

        if(this.options.name === 'price') {
            formatOptions.style = 'currency';
            formatOptions.currency = this.options.currency
        }

        const formattedMinValue = new Intl.NumberFormat(this.options.locale, formatOptions).format(this._inputMin.value)
        const formattedMaxValue = new Intl.NumberFormat(this.options.locale, formatOptions).format(this._inputMax.value)

        let minLabel = `${this.options.snippets.filterRangeActiveMinLabel} ${formattedMinValue}`
        let maxLabel = `${this.options.snippets.filterRangeActiveMaxLabel} ${formattedMaxValue}`

        if(this.options.name !== 'price') {
            minLabel += ` ${this.options.currencySymbol ? this.options.currencySymbol : this.options.unit}`
            maxLabel += ` ${this.options.currencySymbol ? this.options.currencySymbol : this.options.unit}`
        }

        if (this._inputMin.value.length || this._inputMax.value.length) {
            if (this._inputMin.value.length) {
                labels.push({
                    /** @deprecated tag:v6.5.0 - `currencySymbol` will be removed - use `unit` instead */
                    label: minLabel,
                    id: this.options.minKey,
                });
            }

            if (this._inputMax.value.length) {
                labels.push({
                    /** @deprecated tag:v6.5.0 - `currencySymbol` will be removed - use `unit` instead */
                    label: maxLabel,
                    id: this.options.maxKey,
                });
            }
        } else {
            labels = [];
        }

        return labels;
    }

}
