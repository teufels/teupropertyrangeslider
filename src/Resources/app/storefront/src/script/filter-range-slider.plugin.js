import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';
import ViewportDetection from 'src/helper/viewport-detection.helper';

const inputEvent = new Event('input', {
    bubbles: true,
    cancelable: true
});

export default class QuantityField extends Plugin {

    static options =  {
        breakpoints: ['XS', 'SM', 'MD'],
        filterPanelActive: '.filter-panel-active-container'
    };

    static focus = ''

    init() {
        this.loadPlugin();
    }

    loadPlugin() {
        this.sliderA = DomAccess.querySelector(this.el, `.range-slider-a--${this.options.id}`)
        this.sliderB = DomAccess.querySelector(this.el, `.range-slider-b--${this.options.id}`)

        // Set values set in url (if applicable)
        this.setValuesFromUrl();

        this.inputMin = DomAccess.querySelector(document, `.form-control[name=min-${this.options.param}]`);
        this.inputMax = DomAccess.querySelector(document, `.form-control[name=max-${this.options.param}]`);
        this.filterPanelActive = DomAccess.querySelector(document, `${this.options.filterPanelActive}`);

        this.registerEvents()
    }

    registerEvents() {
        this.sliderA.addEventListener('input', this.onChangeValue.bind(this))
        this.sliderB.addEventListener('input', this.onChangeValue.bind(this))

        this.inputMin.addEventListener('keyup', this.onChangeInputValue.bind(this))
        this.inputMax.addEventListener('keyup', this.onChangeInputValue.bind(this))

        this.inputMin.addEventListener('focus', this.registerFocus.bind(this))
        this.inputMax.addEventListener('focus', this.registerFocus.bind(this))

        // Options for the observer (which mutations to observe)
        const config = { childList: true };

        // Create an observer instance linked to the callback function
        const self = this;
        const observer = new MutationObserver(function() { self.onChangeFilters(self.options.param) });

        // Start observing the target node for configured mutations
        observer.observe(this.filterPanelActive, config);
    }

    registerFocus(ev) {
        this.focus = ev.target.className.replace('form-control ', '');
    }

    onChangeInputValue() {
        this.sliderA.value = this.inputMin.value !== '' ? parseFloat(this.inputMin.value) : this.options.min

        if(this.focus === 'min-input') {
            this.inputMax.value === '' ? this.inputMax.value = this.options.max : null
            this.visualizeChange(this.sliderA, this.inputMin.value)
        }else if(this.focus === 'max-input') {
            this.inputMin.value === '' ? this.inputMin.value = this.options.min : null
            this.sliderB.value = this.inputMax.value !== '' ? parseFloat(this.inputMax.value) : this.options.max
            this.visualizeChange(this.sliderB, this.inputMax.value)
        }
    }

    onChangeFilters(param) {
        const params = {};

        setTimeout(() => {
            window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                params[decodeURI(key)] = value
            });

            if (!params[`min-${param}`]) {
                this.sliderA.value = parseFloat(this.options.min)
                this.visualizeChange(this.sliderA)
            }

            if (!params[`max-${param}`]) {
                this.sliderB.value = parseFloat(this.options.max)
                this.visualizeChange(this.sliderB)
            }
        },300)
    }

    onChangeValue(ev) {
        const element = ev.target

        this.visualizeChange(element)

        const valueA = parseFloat(this.sliderA.value)
        const valueB = parseFloat(this.sliderB.value)

        const minValue = valueA < valueB ? valueA : valueB
        const maxValue = valueA < valueB ? valueB : valueA

        let dispatchEvent = false;
        if(this.inputMin.value !== minValue) {
            this.inputMin.value = minValue
            dispatchEvent = true
        }

        if(this.inputMax.value !== maxValue) {
            this.inputMax.value = maxValue
            dispatchEvent = true
        }

        if(dispatchEvent) this.inputMin.dispatchEvent(inputEvent)
    }

    setValuesFromUrl() {
        const params = {};

        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            params[decodeURI(key)] = value;
        });

        if (params[`min-${this.options.param}`]) {
            this.sliderA.value = parseFloat(params[`min-${this.options.param}`])
            this.visualizeChange(this.sliderA)
        }

        if(params[`max-${this.options.param}`]) {
            this.sliderB.value = parseFloat(params[`max-${this.options.param}`])
            this.visualizeChange(this.sliderB)
        }
    }

    activatePluginOnLoad() {
        return !this.options.breakpoints.includes(ViewportDetection.getCurrentViewport());
    }

    visualizeChange(element, value = null) {
        value === null ? value = element.value : null

        const slider = element.dataset.slider
        const decimals = this.options.decimals;

        const formatOptions = {
            minimumFractionDigits: decimals,
            maximumFractionDigits: decimals
        }

        if(this.options.name === 'price') {
            formatOptions.style = 'currency';
            formatOptions.currency = this.options.currency
        }

        const formattedValue = new Intl.NumberFormat(this.options.locale, formatOptions).format(value)

        element.parentNode.style.setProperty(`--value-${slider}`, value);
        element.parentNode.style.setProperty(`--text-value-${slider}`, JSON.stringify(formattedValue))
    }

}
