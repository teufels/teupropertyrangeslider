const { Component, Utils, Mixin, Context, Data } = Shopware;
const { Criteria } = Data;

Component.override('sw-property-detail', {

    inject: ['repositoryFactory'],

    computed: {
        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        customFieldSetOwnCriteria() {
            const criteria = new Criteria();

            criteria.addFilter(Criteria.equals('relations.entityName', 'property_group'));
            criteria.addFilter(Criteria.not(
                'AND',
                [Criteria.equals('name', 'teu_custom_prop_rangeslider_set')],
            ));


            criteria
                .getAssociation('customFields')
                .addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },
    },

    methods: {
        loadCustomFieldSets() {
            /*this.customFieldDataProviderService.getCustomFieldSets('property_group').then((sets) => {
                this.customFieldSets = sets;
            });*/
            this.customFieldSetRepository.search(this.customFieldSetOwnCriteria, Context.api).then((response) => {
                if (response.length) {
                    this.customFieldSets = response;
                }
            });
        },
    }
});