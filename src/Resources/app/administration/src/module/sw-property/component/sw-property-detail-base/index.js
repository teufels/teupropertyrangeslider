import template from './sw-custom-property-detail-base.html.twig';

const { Component } = Shopware;

Component.override('sw-property-detail-base', {
    template,
    data() {
        return {
            key: '',
            unit: '',
            steps: '',
            decimals: '',
            displayForConfigurator: 'dropdown',
            useSingleRange: false
        }
    },
    beforeMount() {
        if(!this.propertyGroup.customFields) {
            this.propertyGroup.customFields = {}
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_unit) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_unit = "";
        }else {
            this.unit = this.propertyGroup.customFields.teu_cat_prop_rangeslider_unit;
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_key) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_key = "";
        }else {
            this.key = this.propertyGroup.customFields.teu_cat_prop_rangeslider_key;
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_steps) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_steps = 1;
        }else {
            this.steps = this.propertyGroup.customFields.teu_cat_prop_rangeslider_steps;
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_decimals) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_decimals = 0;
        }else {
            this.decimals = this.propertyGroup.customFields.teu_cat_prop_rangeslider_decimals;
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_displayForConfigurator) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_displayForConfigurator = 'dropdown';
        }else {
            this.displayForConfigurator = this.propertyGroup.customFields.teu_cat_prop_rangeslider_displayForConfigurator;
        }

        if(!this.propertyGroup.customFields.teu_cat_prop_rangeslider_useSingleRange) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_useSingleRange = false;
        }else {
            this.useSingleRange = this.propertyGroup.customFields.teu_cat_prop_rangeslider_useSingleRange;
        }

        this.displayTypes.push( { value: 'multi-range', label: this.$tc('sw-property.detail.sliderSortingType') } )
    },
    watch: {
        key(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_key = newVal;
        },
        unit(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_unit = newVal;
        },
        steps(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_steps = newVal;
        },
        decimals(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_decimals = newVal;
        },
        displayForConfigurator(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_displayForConfigurator = newVal;
        },
        useSingleRange(newVal) {
            this.propertyGroup.customFields.teu_cat_prop_rangeslider_useSingleRange = newVal;
        }
    }
});
