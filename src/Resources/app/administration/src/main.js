
// Create additional value display type fields
import './module/sw-property/component/sw-property-detail-base/index';
import './module/sw-property/page/sw-property-detail/index';

import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
