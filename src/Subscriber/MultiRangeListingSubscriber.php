<?php

namespace TeuPropertyRangeSlider\Subscriber;

use Doctrine\DBAL\Connection;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\Product\Events\ProductListingResultEvent;
use Shopware\Core\Content\Product\Events\ProductSearchCriteriaEvent;
use Shopware\Core\Framework\Context;

use Shopware\Core\Framework\DataAbstractionLayer\Doctrine\FetchModeHelper;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Bucket\FilterAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Bucket\TermsAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;

use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

class MultiRangeListingSubscriber implements EventSubscriberInterface
{

    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @var EntityRepository
     */
    private EntityRepository $propertyGroupRepository;

    public function __construct(
        Connection $connection,
        EntityRepository $propertyGroupRepository
    ) {
        $this->connection = $connection;
        $this->propertyGroupRepository = $propertyGroupRepository;

    }

    /**
     * @return \array[][]
     */
    #[ArrayShape([
        ProductListingCriteriaEvent::class => "array[]",
        ProductSearchCriteriaEvent::class => "array[]"
    ])] public static function getSubscribedEvents(): array
    {
        return [
            ProductListingCriteriaEvent::class => [
                ['handleListingRequest', 200 ]
            ],
            ProductSearchCriteriaEvent::class => [
                ['handleSearchRequest', 200]
            ]
        ];
    }

    /**
     * @param ProductListingCriteriaEvent $event
     */
    public function handleListingRequest(ProductListingCriteriaEvent $event)
    {
        $request = $event->getRequest();
        $criteria = $event->getCriteria();
        $context = $event->getContext();

        $this->handleFilters($request, $criteria, $context);
    }

    /**
     * @param ProductListingResultEvent $event
     */
    public function handleSearchRequest(ProductSearchCriteriaEvent $event): void
    {
        $request = $event->getRequest();
        $criteria = $event->getCriteria();
        $context = $event->getContext();

        $this->handleFilters($request, $criteria, $context);
    }

    /**
     * @param Request  $request
     * @param Criteria $criteria
     * @param Context  $context
     */
    private function handleFilters(Request $request, Criteria $criteria, Context $context): void
    {
        $this->handleMultiRangeFilter($request, $criteria, $context);
    }

    /**
     * @param Request  $request
     * @param Criteria $criteria
     * @param Context  $context
     */
    private function handleMultiRangeFilter(Request $request, Criteria $criteria, Context $context): void
    {
        $hasMultiRangeFilter = false;
        $filteredProperties = [];

        // get all available property groups and it's options
        $propertyCriteria = new Criteria();
        $propertyCriteria->addAssociation('options');
        $propertyCriteria->addFilter(new EqualsFilter('displayType', 'multi-range'));

        /** @var \Shopware\Core\Content\Property\PropertyGroupEntity $propertyGroup */
        foreach ($this->propertyGroupRepository->search($propertyCriteria, $context)->getElements() as $propertyGroup) {

            if(isset($propertyGroup->getCustomFields()['teu_cat_prop_rangeslider_key']) && !empty($propertyGroup->getCustomFields()['teu_cat_prop_rangeslider_key'])) {
                $key = $propertyGroup->getCustomFields()['teu_cat_prop_rangeslider_key'];
            }else {
                $key = strtolower($propertyGroup->getName());
                $key = preg_replace("/[^[:alnum:]]/",'', $key);
            }

            $min = $request->get('min-'.$key);
            $max = $request->get('max-'.$key);

            if ($min !== null && $max !== null) {
                $hasMultiRangeFilter = true;

                /** @var \Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity $option */
                foreach ($propertyGroup->getOptions()->getElements() as $option) {

                    if($this->propertyMatches($option, $min, $max)) {
                        $filteredProperties[] = $option->getId();
                    }
                    if($option->getTranslated()['name'] === 'empty_fitler') {
                        $filteredProperties[] = $option->getId();
                    }
                }
            }
        }

        if($hasMultiRangeFilter===true) {

            // start the filtering
            $criteria->addAggregation(
                new FilterAggregation(
                    'filterable-properties',
                    new TermsAggregation('properties', 'product.properties.id'),
                    [new EqualsFilter('product.properties.group.filterable', true)]
                )
            );

            $grouped = $this->connection->fetchAllAssociative(
                'SELECT LOWER(HEX(property_group_id)) as property_group_id, LOWER(HEX(id)) as id FROM property_group_option WHERE id IN (:ids)',
                ['ids' => Uuid::fromHexToBytesList($filteredProperties)],
                ['ids' => Connection::PARAM_STR_ARRAY]
            );

            $grouped = FetchModeHelper::group($grouped);
            foreach ($grouped as $options) {
                $options = array_column($options, 'id');

                $criteria->addPostFilter(
                    new MultiFilter(
                        MultiFilter::CONNECTION_OR,
                        [
                            new EqualsAnyFilter('product.propertyIds', $options),
                            new EqualsAnyFilter('product.optionIds', $options),
                        ]
                    )
                );
            }
        }

    }

    /**
     * @param \Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity $option
     * @param $min
     * @param $max
     * @return bool
     */
    #[Pure] protected function propertyMatches(\Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity $option, $min, $max): bool
    {
        return floatval($option->getTranslated()['name']) >= $min && floatval($option->getTranslated()['name']) <= $max;
    }
}
